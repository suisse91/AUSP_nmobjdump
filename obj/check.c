/*
** main.c for my_objdump in /home/amstuta/rendu/AUSP_nmobjdump/obj
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Wed Apr  8 14:16:12 2015 arthur
** Last update Wed Apr  8 14:16:35 2015 arthur
*/

#include "objdump.h"

eBool	check_file(Elf64_Ehdr *elf)
{
  if (elf->e_ident[EI_MAG0] != ELFMAG0 ||
      elf->e_ident[EI_MAG1] != ELFMAG1 ||
      elf->e_ident[EI_MAG2] != ELFMAG2 ||
      elf->e_ident[EI_MAG3] != ELFMAG3 ||
      elf->e_ident[EI_CLASS] != ELFCLASS64 ||
      (elf->e_type != ET_EXEC && elf->e_type
       != ET_REL && elf->e_type != ET_DYN))
    return (false);
  return (true);
}
