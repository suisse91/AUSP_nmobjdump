/*
** main.c for my_objdump in /home/amstuta/rendu/AUSP_nmobjdump/obj
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Wed Apr  8 14:16:12 2015 arthur
** Last update Wed Apr  8 14:16:35 2015 arthur
*/

#ifndef OBJDUMP_H_
# define OBJDUMP_H_

# include <elf.h>

# define HAS_RELOC	0x01
# define EXEC_P		0x02
# define HAS_SYMS	0x10
# define DYNAMIC	0x40
# define D_PAGED	0x100

typedef enum
  {
    true,
    false
  }	eBool;

void	open_file(char*);
void	print_sections(Elf64_Shdr*, char*, int, void*);
void	read_section(void*, Elf64_Shdr*, int);
void	print_spaces(int);
void	print_text(void*, uint32_t);
void	print_header(char*, Elf64_Ehdr*, Elf64_Shdr*);
void	print_flags(Elf64_Ehdr*, Elf64_Shdr*);
int	get_flags(Elf64_Ehdr*, Elf64_Shdr*);
eBool	is_printable(char);
int	filesize(int);
eBool	check_file(Elf64_Ehdr*);

#endif /* !OBJDUMP_H_ */
