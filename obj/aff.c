/*
** main.c for my_objdump in /home/amstuta/rendu/AUSP_nmobjdump/obj
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Wed Apr  8 14:16:12 2015 arthur
** Last update Wed Apr  8 14:16:35 2015 arthur
*/

#include <stdio.h>
#include "objdump.h"

eBool		is_printable(char c)
{
  if (c >= 32 && c <= 126)
    return (true);
  return (false);
}

void		print_text(void *data, uint32_t size)
{
  uint32_t	idx;
  char		*tmp;

  idx = 0;
  printf(" ");
  while (idx < size)
    {
      tmp = data + idx;
      if (is_printable(*tmp) == true)
	printf("%c", *tmp);
      else
	printf(".");
      ++idx;
    }
}

void		print_spaces(int idx)
{
  uint32_t	spaces;
  
  spaces = idx % 16;
  while (spaces < 16)
    {
      printf("  ");
      if (spaces % 4 == 0)
	printf(" ");
      ++spaces;
    }
}

void		print_flags(Elf64_Ehdr *elf, Elf64_Shdr *shdr)
{
  int		i;

  i = -1;
  if (elf->e_type == ET_EXEC)
    printf("EXEC_P ");
  if (elf->e_type == ET_REL)
    printf("HAS_RELOC ");
  while (++i < elf->e_shnum)
    {
      if (shdr[i].sh_type == SHT_SYMTAB)
	printf("HAS_SYMS ");
    }
  if (elf->e_type == ET_DYN)
    printf("HAS_SYMS DYNAMIC ");
  if (elf->e_phnum > 0)
    printf("D_PAGED ");
}

void		print_header(char *filename, Elf64_Ehdr *info, Elf64_Shdr *shdr)
{
  printf("\n%s:%5cfile format %s\n", filename, ' ',
	 info->e_type == ELFCLASS64 ? "elf64-x86-64" : "elf32-x86-64");
  printf("architecture: %s, flags 0x%08x:\n",
	 info->e_machine == EM_X86_64 ? "i386:x86-64" : "unknown",
	 get_flags(info, shdr));
  print_flags(info, shdr);
  printf("\n");
  printf("start address 0x%016x\n", (unsigned int)info->e_entry);
}
