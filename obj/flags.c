/*
** main.c for my_objdump in /home/amstuta/rendu/AUSP_nmobjdump/obj
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Wed Apr  8 14:16:12 2015 arthur
** Last update Wed Apr  8 14:16:35 2015 arthur
*/

#include "objdump.h"

int		get_flags(Elf64_Ehdr *elf, Elf64_Shdr *shdr)
{
  int		i;
  int		res;

  i = -1;
  res = 0;
  if (elf->e_type == ET_EXEC)
    res |= EXEC_P;
  if (elf->e_type == ET_DYN)
    res |= DYNAMIC | HAS_SYMS;
  if (elf->e_phnum > 0)
    res |= D_PAGED;
  if (elf->e_type == ET_REL)
    res |= HAS_RELOC;
  while (++i < elf->e_shnum)
    {
      if (shdr[i].sh_type == SHT_SYMTAB)
	res |= HAS_SYMS;
    }
  return (res);
}
