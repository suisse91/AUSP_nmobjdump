/*
** main.c for my_objdump in /home/amstuta/rendu/AUSP_nmobjdump/obj
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Wed Apr  8 14:16:12 2015 arthur
** Last update Wed Apr  8 14:16:35 2015 arthur
*/

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include "objdump.h"

int		filesize(int fd)
{
  return (lseek(fd, 0, SEEK_END));
}

void		read_section(void *data, Elf64_Shdr *shdr, int i)
{
  uint32_t	idx;
  unsigned char	*val;

  idx = 0;
  while (idx < shdr[i].sh_size)
    {
      if (idx % 16 == 0)
	printf("%06x", (unsigned int)(shdr[i].sh_addr + idx));
      if (idx % 4 == 0)
	printf(" ");
      val = data + shdr[i].sh_offset + idx;
      printf("%02x", *val);
      ++idx;
      if (idx % 16 == 0)
	{
	  print_text((void*)(data + shdr[i].sh_offset + idx - 16), 16);
	  printf("\n");
	}
    }
  if (idx % 16 != 0)
    {
      print_spaces(idx);
      print_text((void*)(data + shdr[i].sh_offset + idx - (idx % 16)), (idx % 16));
    }
}

void		print_sections(Elf64_Shdr *shdr, char *strtab, int shnum, void *data)
{
  int		i;

  i = 0;
  while (i < shnum)
    {
      if (strlen(&strtab[shdr[i].sh_name]) != 0
	  && strcmp(&strtab[shdr[i].sh_name], ".strtab")
	  && strcmp(&strtab[shdr[i].sh_name], ".symtab")
	  && strcmp(&strtab[shdr[i].sh_name], ".shstrtab")
	  && strcmp(&strtab[shdr[i].sh_name], ".bss"))
	{
	  printf("\nContent of section %s:\n", &strtab[shdr[i].sh_name]);
	  read_section(data, shdr, i);
	}
      ++i;
    }
  printf("\n");
}

void		open_file(char *file)
{
  void		*data;
  Elf64_Shdr	*shdr;
  Elf64_Ehdr	*elf;
  int		fd;
  char		*strtab;

  if ((fd = open(file, O_RDONLY)) == -1 ||
      (data = mmap(NULL, filesize(fd), PROT_READ, MAP_SHARED, fd, 0))
      == MAP_FAILED)
    {
      printf("Error: file \"%s\" doesn't exist or couldn't be mmapped\n",
	     file);
      return ;
    }
  elf = (Elf64_Ehdr*)data;
  if (check_file(elf) == false)
    {
      printf("Error: not elf format\n");
      return ;
    }
  shdr = (Elf64_Shdr*)(data + elf->e_shoff);
  strtab = (char*)(data + shdr[elf->e_shstrndx].sh_offset);
  print_header(file, elf, shdr);
  print_sections(shdr, strtab, elf->e_shnum, data);
}

int		main(int ac, char **av)
{
  int		i;

  i = 1;
  if (ac == 1)
    {
      printf("Usage: ./my_objdump file1 [file2] [...]\n");
      return (EXIT_FAILURE);
    }
  while (i < ac)
    {
      open_file(av[i]);
      ++i;
    }
  return (EXIT_SUCCESS);
}
