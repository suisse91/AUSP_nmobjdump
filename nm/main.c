/*
** main.c for my_nm in /home/amstuta/rendu/AUSP_nmobjdump/nm
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Wed Apr  8 14:15:27 2015 arthur
** Last update Wed Apr  8 15:53:58 2015 arthur
*/

#include <elf.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

int	filesize(int fd)
{
  return (lseek(fd, 0, SEEK_END));
}

void	print_sh_name(Elf64_Shdr *shdr, char *strtab, int shnum)
{
  int	i;

  i = 0;
  while (i < shnum)
    {
      printf("%02d: %s\n", i, &strtab[shdr[i].sh_name]);
      ++i;
    }
}

int	main(int ac, char **av)
{
  (void)ac;
  (void)av;
  void		*data;
  Elf64_Ehdr	*elf;
  Elf64_Shdr	*shdr;
  int		fd;
  char		*strtab;

  fd = open(av[1], O_RDONLY);
  data = mmap(NULL, filesize(fd), PROT_READ, MAP_SHARED, fd, 0);
  elf = (Elf64_Ehdr*)data;
  shdr = (Elf64_Shdr*)(data + elf->e_shoff);
  strtab = (char*)(data + shdr[elf->e_shstrndx].sh_offset);
  print_sh_name(shdr, strtab, elf->e_shnum);

  return (0);
}
