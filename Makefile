##
## Makefile for makefile in /home/amstuta/rendu/AUSP_nmobjdump
##
## Made by arthur
## Login   <amstuta@epitech.net>
##
## Started on  Wed Apr  8 14:08:50 2015 arthur
## Last update Thu Apr  9 16:39:57 2015 arthur
##

RM	= rm -f

CC	= gcc
CFLAGS	= -Wall -Wextra -Werror

SRCS1	= nm/main.c
SRCS2	= obj/main.c \
	  obj/aff.c \
	  obj/check.c \
	  obj/flags.c

NAME	= my_nm
NAME1	= my_objdump

OBJS1	= $(SRCS1:.c=.o)
OBJS2	= $(SRCS2:.c=.o)

all:	   $(NAME1) $(NAME)

$(NAME):   $(OBJS1)
	   $(CC) $(OBJS1) -o $(NAME)

$(NAME1):  $(OBJS2)
	   $(CC) $(OBJS2) -o $(NAME1)

nm:	   $(NAME)

objdump:   $(NAME1)

clean:
	   $(RM) $(OBJS1) $(OBJS2)

fclean:	   clean
	   $(RM) $(NAME) $(NAME1)

re:	   fclean all

.PHONY:	   all clean fclean re
